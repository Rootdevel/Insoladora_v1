
; PIC16F84A Configuration Bit Settings

; Assembly source line config statements

#include "p16f84a.inc"

; CONFIG
; __config 0x3FF2
 __CONFIG _FOSC_HS & _WDTE_OFF & _PWRTE_ON & _CP_OFF

 
RES_VECT  CODE    0x0000            ; processor reset vector
   GOTO    START                   ; go to beginning of program
    
INTE_VECT CODE 0x0004
    GOTO    INTER
	
	
	
    


;MAIN_PROG CODE                      ; let linker place main program


CBLOCK	0X20
	REG1
	REG2
	REG3
	REG4
	SEGUNDOS
	MINUTOS
	CUENTA
	CARAC
	RELCD
	PUERTO
	
ENDC

LIN1	ADDWF	PCL,1
		DT	"INSOLADORA 2.0  "
LIN2	ADDWF	PCL,1
		DT	"  Rootdevel     "
		
INTER
	BCF		INTCON,2
	MOVWF	REG2
	SWAPF	STATUS,W
	MOVWF	REG3
	INCF	REG4,1
	BTFSS	REG4,4			;PREGUNTA SI LLEGO A 16
	GOTO	FIN_INTER
	INCF	SEGUNDOS,1
	CLRF	REG4
	MOVLW	0x3B
	XORWF	SEGUNDOS,W
	BTFSS	STATUS,2
	GOTO	FIN_INTER
	INCF	MINUTOS
	CLRF	SEGUNDOS
	MOVLW	0x3B
	XORWF	MINUTOS,W
	BTFSS	STATUS,2
	GOTO	FIN_INTER
	CLRF	MINUTOS
		
FIN_INTER	
	SWAPF	REG3,W
	MOVWF	STATUS
	MOVF	REG2,W
	RETFIE
	
	
	

START
    BCF	STATUS, RP0 ;
	CLRF	PORTA
    CLRF    PORTB   
	MOVLW	B'10100000'
	MOVWF	INTCON
	MOVLW	0x0E
	MOVWF	TMR0	
	;MOVLW	B'
    BSF	STATUS, RP0 ;Select Bank 1
    MOVLW   0x00   
    MOVWF   TRISB   ;Set RB<7:0> as outputs
    MOVLW	0XFF
	MOVWF	TRISA
	MOVLW	B'00000111'
	MOVWF	OPTION_REG
	BCF	STATUS, RP0 ;
	CALL	IN4_BIT
	
	MOVLW	B'00000001'			;BORRA Y REGRESA A CERO
	CALL	COMAND4	
	
;	CALL	ON_RELAY
;	CALL	ON_BUZZER
	
	CLRF	CARAC
CON1	MOVF	CARAC,0
		CALL	LIN1
		CALL	DATO4
		INCF	CARAC,1
		BTFSS	CARAC,4			;PREGUNTA SI LLEGO A 16
		GOTO	CON1

		MOVLW	B'11000000'		;APUNTADOR A LINEA 2 POSICION CERO
		CALL	COMAND4	

		CLRF	CARAC
CON2	MOVF	CARAC,0
		CALL	LIN2
		CALL	DATO4
		INCF	CARAC,1
		BTFSS	CARAC,4			;PREGUNTA SI LLEGO A 16
		GOTO	CON2
BUCLE
		MOVF	PORTA,W
		MOVWF	REG1
		BTFSC   REG1,0
		GOTO	PAS_2
		CALL	PLAY
PAS_2
		BTFSC	REG1,1
		GOTO	PAS_3
		CALL	STOP
PAS_3
    	BTFSC   REG1,2
		GOTO	PAS_4
		CALL	ADD
PAS_4
		BTFSC	REG1,3
		GOTO	PAS_5
		CALL	SUB
PAS_5
		BTFSC	REG1,4
		GOTO	BUCLE
		CALL	SELECT
		
		GOTO	BUCLE
		
		
		GOTO	$
		
		

;------------------------------------------------------------------

PLAY
	CALL	ON_BUZZER
	RETURN

STOP
	CALL	OFF_BUZZER
	RETURN
	
ADD	
	CALL	ON_RELAY
	RETURN 
SUB
	CALL	OFF_RELAY
	RETURN
	
SELECT
	CALL	OFF_RELAY
	RETURN
			


;-----------RUTINA DE INICIACION DE LCD DE 4 BITS---------------------
IN4_BIT	MOVLW	0X04
		CALL	COMAND4				;ACTIVA DISPLAY
		MOVLW	32			;COMUNICACION A 4 BITS DOS LINEAS FUENTE 5X7
		CALL	COMAND4	
		MOVLW	28			;NO SE DESPLAZA, APUNTADOR SE INCREMENTA CON NUEVA ESCRITURA DE DATO
		CALL	COMAND4	
		MOVLW	0X0C			;BORRA Y REGRESA A CERO
		CALL	COMAND4
		MOVLW	B'00000110'
		CALL	COMAND4
		MOVLW	0X01
		CALL	COMAND4
		RETURN
;------------------------------------------------------------------------		

;------------RUTINA DE ENVIO DE DATOS Y COMANDOS A LCD----
COMAND4	BCF		PORTB,2			;ENVIO DE COMANDO (JULIAN, EL BIT DEL PROTEUS ES EL INVERTIDO DEL DATASHEET)
		GOTO	$+2
DATO4	BSF		PORTB,2			;ENVIO DE DATO
		MOVWF	PUERTO
		MOVLW	B'00001111'
		ANDWF	PORTB,1
		MOVF	PUERTO,0
		ANDLW	B'11110000'
		IORWF	PORTB,1


		BSF		PORTB,3			;HABILITA PANTALLA
		CALL	RETLCD
		BCF		PORTB,3			;DESHABILITA PANTALLA
		CALL	RETLCD

		MOVLW	B'00001111'		;BORRA PARTE ALTA DE PORTB
		ANDWF	PORTB,1

		SWAPF	PUERTO,0
		ANDLW	B'11110000'

		IORWF	PORTB,1
	
		BSF		PORTB,3			;HABILITA PANTALLA
		CALL	RETLCD
		BCF		PORTB,3			;DESHABILITA PANTALLA
		CALL	RETLCD

		RETURN
;----------------------------------------------------------
;-------------RUTINA DE RETARDO PARA LCD----
RETLCD	
	MOVLW	.255
	MOVWF	RELCD
RETLCD1	
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	DECFSZ	RELCD,1
	GOTO	RETLCD1
	RETURN
;-------------------------------------------

;------------ON RELAY-------------------------
ON_RELAY
	BSF	PORTB,1
	RETURN
;---------------------------------------------

;------------OFF RELAY------------------------
OFF_RELAY
	BCF	PORTB,1
	RETURN
;---------------------------------------------

;------------ON BUZZER------------------------
ON_BUZZER	
	BSF	PORTB,0
	RETURN
;---------------------------------------------
;-----------OFF BUZZER------------------------
OFF_BUZZER
	BCF	PORTB,0
	RETURN
;---------------------------------------------

    GOTO $                          ; loop forever

    END